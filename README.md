# automationproj

In this project, I worked on both REST and SOAP APIs using Postman. I automated API tasks and checked them using Postman. I also did testing with static data from .csv and .json files. To make easy-to-read reports and work with CI/CD tools, I used Newman command line with HTML reports


## Getting started

Anyone who wants to use this frameworks can clone and implement their respective scenarios.
Softwares needed:
1) for development : Postman(latest version preferred)(Download link: https://www.postman.com/downloads/)
2) For execution: Node.JS , Newman.(https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/)

## git commands to clone and pull

git clone

git pull

## postman version i have used v11.1.14

## Project status
In progress : I will be adding more APIs examples